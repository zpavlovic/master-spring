# Master thesis#

This is master thesis project for Software Engineering department at Faculty of Organisation Science, Belgrade University. 

### Technologies used###

* Spring Boot
* Spring DATA JPA
* MySQL, HSQL (testing)
* Spring Security
* Tymeleaf
* Bootstrap

### How do I get set up? ###

* Create database from nst2.sql file from the project.
* Change database connection credentials in application.properties file
* Open application.properties file and change location of image.location param to image folder inside repository
* Build and run, then open website at http://localhost:8080