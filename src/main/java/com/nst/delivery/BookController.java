package com.nst.delivery;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import com.nst.data.author.AuthorMapper;
import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryMapper;
import com.nst.data.publisher.PublisherMapper;
import com.nst.data.user.UserMapper;
import com.nst.domain.author.Author;
import com.nst.domain.author.all.GetAllAuthorsInteractor;
import com.nst.domain.book.Book;
import com.nst.domain.book.DeleteBookInteractor;
import com.nst.domain.book.all.GetAllBooksInteractor;
import com.nst.domain.book.get.GetBookInteractor;
import com.nst.domain.book.popular.GetPopularBooksInteractor;
import com.nst.domain.book.recommend.RecommendBooksInteractor;
import com.nst.domain.book.save.SaveBookInteractor;
import com.nst.domain.book.search.SearchBookInteractor;
import com.nst.domain.category.Category;
import com.nst.domain.category.all.GetAllCategoriesInteractor;
import com.nst.domain.category.popular.GetPopularCategoriesInteractor;
import com.nst.domain.publisher.all.GetAllPublishersInteractor;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.user.login.GetLoggedInUserInteractor;
import com.nst.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nst.data.author.AuthorDb;
import com.nst.data.category.CategoryDb;
import com.nst.data.publisher.PublisherDb;

@Controller
public class BookController {

	@Autowired private SaveBookInteractor saveBookInteractor;
	@Autowired private GetPopularBooksInteractor getPopularBooksInteractor;
	@Autowired private GetAllBooksInteractor getAllBooksInteractor;
	@Autowired private GetBookInteractor getBookInteractor;
	@Autowired private RecommendBooksInteractor recommendBooksInteractor;
	@Autowired private SearchBookInteractor searchBookInteractor;
	@Autowired private DeleteBookInteractor deleteBookInteractor;
	@Autowired private GetAllAuthorsInteractor getAllAuthorsInteractor;
	@Autowired private GetAllPublishersInteractor getAllPublishersInteractorInteractor;
	@Autowired private GetPopularCategoriesInteractor getPopularCategoriesInteractor;
	@Autowired private GetAllCategoriesInteractor getAllCategoriesInteractor;

	@Autowired private GetLoggedInUserInteractor getLoggedInUserInteractor;

	@Value("${image.location}") private String imageLocation;

	private final ResourceLoader resourceLoader;

	@Autowired
	public BookController(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	@RequestMapping(value="add_book", method = RequestMethod.GET)
	public String addAuthor(Model model){
		model.addAttribute(new BookDb());
		model.addAllAttributes(displayAllAuthors());
		return "add_book";
	}

	@RequestMapping(value="action/add_book", method = RequestMethod.POST)
	public String addBook(@Valid @ModelAttribute BookDb book, BindingResult result, MultipartFile file, Model model) throws Exception {
		if(result.hasErrors()){
			return "add_book";
		}
		
		BookDb bookForSave = setUpBook(book, result, file);
		BookDb bookWithImage = handleBookImage(bookForSave, file);
		Book cleanBook = BookMapper.convertToPlainModel(bookWithImage);
		Book savedBook = saveBookInteractor.execute(cleanBook);

		model.addAttribute(BookMapper.convertToDbModel(savedBook));
		model.addAllAttributes(displayAllBooks());

		if(savedBook == null){
			return "add_book";
		}		

		return "redirect:/index";
	}

	@RequestMapping(value = "/image/{imageName}")
	@ResponseBody
	public byte[] getFile(@PathVariable String imageName) throws IOException {		   
		Resource resource = resourceLoader.getResource("file:" + Paths.get(imageLocation, imageName+".jpg"));
		File file = resource.getFile();	
		return Files.readAllBytes(file.toPath());
	}

	@RequestMapping(value="/search", method = RequestMethod.POST)
	public String searchBook(@RequestParam String name, Model model) throws Exception {
		try {
			List<Book> books = searchBookInteractor.execute(name);
			List<BookDb> searchBooks = BookMapper.convertToListOfDbModels(books);
			model.addAttribute("searchBooks",searchBooks);
			model.addAllAttributes(getPopularBooks());
			model.addAttribute("book", new BookDb());
			model.addAttribute("popularCategories", getPopularCategories());
			return "search";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("book", new BookDb());
			model.addAttribute("error", e.getMessage());
			model.addAttribute("popularCategories", getPopularCategories());
			return "search";
		}			
	}

	@RequestMapping(value ="book", method = RequestMethod.GET)
	public String book(Model model, int id) throws Exception {
		// Get Single Book for showing it on the screen
		Book book = getBookInteractor.execute(id);
		//Increase book views counter
		BookDb savedBook = increaseBookViews(book);

		//Get authors and categories for showing on the screen
		List<AuthorDb> authors = getAllAuthorsForBook(savedBook);
		List<CategoryDb> categories = getAllCategoriesForBook(savedBook);

		model.addAllAttributes(getPopularBooks());
		model.addAllAttributes(getRecommendBooks());
		model.addAttribute("book", savedBook);
		model.addAttribute("authors", authors);
		model.addAttribute("categories", categories);
		model.addAttribute("publisher", savedBook.getPublisher());
		model.addAttribute("popularCategories", getPopularCategories());

		return "book";
	}

	@RequestMapping(value ="/action/deleteBook", method = RequestMethod.GET)
	public String deleteBook(Model model, int id) throws Exception {
		deleteBookInteractor.execute(id);
		return "redirect:/index";
	}

	@RequestMapping(value ="/editBook", method = RequestMethod.GET)
	public String editBook(Model model, int id){
		Book book = getBookInteractor.execute(id);
		BookDb editBook = BookMapper.convertToDbModel(book);
		model.addAttribute("book", editBook);
		return "edit_book";		
	}

	@ModelAttribute("authors")
	public List<AuthorDb> displayAllAuthors() {
		List<Author> authors = getAllAuthorsInteractor.execute(null);
		return AuthorMapper.convertToListOfDbModels(authors);
	}

	@ModelAttribute("categories")
	public List<CategoryDb> displayAllCategories() throws Exception {
		List<Category> categories = getAllCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categories);
	}	
	
	@ModelAttribute("publishers")
	public List<PublisherDb> displayAllPublishers() throws Exception {
		List<Publisher> publishers = getAllPublishersInteractorInteractor.execute(null);
		return PublisherMapper.convertToListOfDbModels(publishers);
	}	

	@ModelAttribute("books")
	public List<BookDb> displayAllBooks() {
		List<Book> books = getAllBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}

	@ModelAttribute("popularCategories")
	public List<CategoryDb> getPopularCategories() throws Exception {
		List<Category> categories = getPopularCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categories);
	}

	@ModelAttribute("popularBooks")
	public List<BookDb> getPopularBooks() {
		List<Book> books = getPopularBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}

	@ModelAttribute("recommendBooks")
	public List<BookDb> getRecommendBooks() {
		List<Book> books = recommendBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	private BookDb handleBookImage(BookDb book, MultipartFile file) {
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(imageLocation, file.getOriginalFilename()));
				book.setImage(file.getOriginalFilename());
				System.out.println("LOKACIJA: "+ imageLocation +"/"+file.getOriginalFilename());
			} catch (IOException|RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
		return book;
	}

	private BookDb setUpBook(BookDb book, BindingResult result, MultipartFile file) throws Exception {
		User currentUser = getLoggedInUserInteractor.execute(true);
		book.setUser(UserMapper.convertToDbModel(currentUser));
		book.setAddedDate(getAddedDate());
		return book;
	}
	
	private Date getAddedDate() {
		Calendar calendar = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(calendar.getTime().getTime());    
		return date;
	}
	

	private BookDb increaseBookViews(Book book) {
		long views = book.getViews();
		views++;
		book.setViews(views);
		return BookMapper.convertToDbModel(book);
	}

	private List<CategoryDb> getAllCategoriesForBook(BookDb savedBook) {
		//Get all categories for this book
		List<CategoryDb> categories = new ArrayList<>();

		for(CategoryDb c : savedBook.getCategoryList()){
			categories.add(c);
		}
		return categories;
	}

	private List<AuthorDb> getAllAuthorsForBook(BookDb savedBook) {
		//Get all authors for this book
		List<AuthorDb> authors = new ArrayList<>();

		for(AuthorDb a : savedBook.getAuthorsList()){
			authors.add(a);
		}
		return authors;
	}
}
