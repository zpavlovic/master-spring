package com.nst.delivery;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryMapper;
import com.nst.data.publisher.PublisherMapper;
import com.nst.data.user.UserMapper;
import com.nst.domain.book.Book;
import com.nst.domain.book.all.GetAllBooksInteractor;
import com.nst.domain.book.popular.GetPopularBooksInteractor;
import com.nst.domain.category.Category;
import com.nst.domain.category.popular.GetPopularCategoriesInteractor;
import com.nst.domain.publisher.all.GetAllPublishersInteractor;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.delete.DeletePublisherInteractor;
import com.nst.domain.publisher.save.SavePublisherInteractor;
import com.nst.domain.publisher.get.GetPublisherInteractor;
import com.nst.domain.user.login.GetLoggedInUserInteractor;
import com.nst.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nst.data.publisher.PublisherDb;

@Controller
public class PublisherController {

	@Autowired GetPublisherInteractor getPublisherInteractor;
	@Autowired GetLoggedInUserInteractor getLoggedInUserInteractor;
	@Autowired GetPopularBooksInteractor getPopularBooksInteractor;
	@Autowired GetAllBooksInteractor getAllBooksInteractor;
	@Autowired GetPopularCategoriesInteractor getPopularCategoriesInteractor;
	@Autowired GetAllPublishersInteractor getAllPublishersInteractor;
	@Autowired SavePublisherInteractor savePublisherInteractor;
	@Autowired DeletePublisherInteractor deletePublisherInteractor;
	
	@RequestMapping(value="/add_publisher", method = RequestMethod.GET)
	public String addPublisher(Model model){
		model.addAttribute(new PublisherDb());
		return "add_publisher";
	}

	@RequestMapping(value="/action/add_publisher", method = RequestMethod.POST)
	public String addPublisher(@Valid @ModelAttribute PublisherDb publisher, BindingResult bindingResult, Model model) throws Exception {

		if(bindingResult.hasErrors()){
			return "add_publisher";
		}

		PublisherDb publisherForSave = getPublisherForLoggedInUser(publisher);

		Publisher savedPublisher = savePublisherInteractor.execute(PublisherMapper.convertToPlainModel(publisherForSave));

		model.addAttribute(PublisherMapper.convertToDbModel(savedPublisher));
		model.addAttribute("book", new BookDb());
		model.addAllAttributes(getAllBooks());
		model.addAttribute("popularCategories", getPopularCategories());

		if(savedPublisher == null){
			return "add_publisher";
		}		

		return "redirect:/index";
	}

	@RequestMapping(value = ("/publishers"), method = RequestMethod.GET)
	public String categories(Model model) throws Exception {
		List<Publisher> publishers = getAllPublishersInteractor.execute(null);
		model.addAttribute("publishers", PublisherMapper.convertToListOfDbModels(publishers));
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_publishers";
	}


	@RequestMapping(value ="publisher", method = RequestMethod.GET)
	public String view(Model model, long id) throws Exception {
		Publisher publisher = getPublisherInteractor.execute((int) id);

		List<BookDb> books = getAlLBooksForPublisher(PublisherMapper.convertToDbModel(publisher));

		// Add models
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("publisherBooks", books);
		model.addAttribute("publisher", publisher);
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());

		return "publisher";
	}

	@RequestMapping(value ="/action/deletePublisher", method = RequestMethod.GET)
	public String deletePublisher(Model model, int id) throws Exception {
		deletePublisherInteractor.execute(id);
		return "redirect:/index";
	}

	@RequestMapping(value ="/editPublisher", method = RequestMethod.GET)
	public String editBook(Model model, int id) throws Exception {
		Publisher editPublisher = getPublisherInteractor.execute(id);
		model.addAttribute("publisher", PublisherMapper.convertToDbModel(editPublisher));
		return "edit_publisher";		
	}	

	@ModelAttribute("books")
	public List<BookDb> getAllBooks() {
		List<Book> books = getAllBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}

	@ModelAttribute("popularBooks")
	public List<BookDb> getPopularBooks() {
		List<Book> books = getPopularBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}

	@ModelAttribute("popularCategories")
	public List<CategoryDb> getPopularCategories() throws Exception {
		List<Category> categories = getPopularCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categories);
	}	

	private List<BookDb> getAlLBooksForPublisher(PublisherDb publisher) {
		//Find books for this category
		List<BookDb> books = new ArrayList<>();
		for(BookDb b : publisher.getBookList()){
			books.add(b);
		}
		return books;
	}	

	private PublisherDb getPublisherForLoggedInUser(PublisherDb publisher) throws Exception {
		User user = getLoggedInUserInteractor.execute(true);
		publisher.setUser(UserMapper.convertToDbModel(user));
		return publisher;
	}

}
