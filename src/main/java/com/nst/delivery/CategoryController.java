package com.nst.delivery;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryMapper;
import com.nst.data.user.UserMapper;
import com.nst.domain.book.Book;
import com.nst.domain.book.all.GetAllBooksInteractor;
import com.nst.domain.book.popular.GetPopularBooksInteractor;
import com.nst.domain.category.Category;
import com.nst.domain.category.DeleteCategoryInteractor;
import com.nst.domain.category.all.GetAllCategoriesInteractor;
import com.nst.domain.category.popular.GetPopularCategoriesInteractor;
import com.nst.domain.category.save.SaveCategoryInteractor;
import com.nst.domain.category.get.GetCategoryInteractor;
import com.nst.domain.user.login.GetLoggedInUserInteractor;
import com.nst.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CategoryController {

	@Autowired GetAllCategoriesInteractor getAllCategoriesInteractor;
	@Autowired GetPopularCategoriesInteractor getPopularCategoriesInteractor;
	@Autowired GetPopularBooksInteractor getPopularBooksInteractor;
	@Autowired GetLoggedInUserInteractor getLoggedInUserInteractor;
	@Autowired GetAllBooksInteractor getAllBooksInteractor;
	@Autowired SaveCategoryInteractor saveCategoryInteractor;
	@Autowired DeleteCategoryInteractor deleteCategoryInteractor;
	@Autowired GetCategoryInteractor getCategoryInteractor;

	@RequestMapping(value="/add_category", method = RequestMethod.GET)
	public String addCategory(Model model){
		model.addAttribute(new CategoryDb());
		return "add_category";
	}
	
	@RequestMapping(value="/action/add_category", method = RequestMethod.POST)
	public String addCategory(@Valid @ModelAttribute CategoryDb category, BindingResult bindingResult, Model model) throws Exception {
		
		if(bindingResult.hasErrors()){
			return "add_category";
		}	
		
		CategoryDb categoryForSave = getCategoryForLoggedInUser(category);
		
		Category savedCategory = saveCategoryInteractor.execute(CategoryMapper.convertToPlainModel(categoryForSave));
		
		model.addAttribute(category);
		model.addAttribute("book", new BookDb());
		model.addAllAttributes(getAllBooks());
		
		if(savedCategory == null){
			return "add_category";
		}		
		
		return "redirect:/index";
	}	

	@RequestMapping(value ="category", method = RequestMethod.GET)
	public String category(Model model, long id) throws Exception {
		Category category = getCategoryInteractor.execute((int) id);
		
		//Find books for this category
		List<BookDb> books = getBooksForCategory(CategoryMapper.convertToDbModel(category));
		
		// Add models
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("categoryBooks", books);
		model.addAttribute("category", category);
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());
		return "category";
	}

	@RequestMapping(value ="/action/deleteCategory", method = RequestMethod.GET)
	public String deleteBook(Model model, int id) throws Exception {
		deleteCategoryInteractor.execute(id);
		return "redirect:/index";
	}
	
	@RequestMapping(value ="/editCategory", method = RequestMethod.GET)
	public String editBook(Model model, int id) throws Exception {
		Category category = getCategoryInteractor.execute(id);
		model.addAttribute("category", CategoryMapper.convertToDbModel(category));
		return "edit_category";		
	}
	
	@RequestMapping(value = ("/categories"), method = RequestMethod.GET)
	public String categories(Model model) throws Exception {
		List<Category> categories = getAllCategoriesInteractor.execute(null);
		model.addAttribute("categories", CategoryMapper.convertToListOfDbModels(categories));
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_categories";
	}
	
	@ModelAttribute("books")
	public List<BookDb> getAllBooks() {
		List<Book> books = getAllBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	@ModelAttribute("popularBooks")
	public List<BookDb> getPopularBooks() {
		List<Book> books = getPopularBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	@ModelAttribute("popularCategories")
	public List<CategoryDb> getPopularCategories() throws Exception {
		List<Category> categorys = getPopularCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categorys);
	}
	
	private CategoryDb getCategoryForLoggedInUser(CategoryDb category) throws Exception {
		User currentUser = getLoggedInUserInteractor.execute(true);
		category.setUser(UserMapper.convertToDbModel(currentUser));
		return category;
	}
		
	private List<BookDb> getBooksForCategory(CategoryDb category) {
		List<BookDb> books = new ArrayList<>();
		for(BookDb b : category.getBookList()){
			books.add(b);
		}
		return books;
	}
}
