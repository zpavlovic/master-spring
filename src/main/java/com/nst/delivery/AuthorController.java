package com.nst.delivery;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.nst.data.author.AuthorDb;
import com.nst.data.author.AuthorMapper;
import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryMapper;
import com.nst.data.user.UserMapper;
import com.nst.domain.author.Author;
import com.nst.domain.author.EditAuthorInteractor;
import com.nst.domain.author.all.GetAllAuthorsInteractor;
import com.nst.domain.author.delete.DeleteAuthorInteractor;
import com.nst.domain.author.save.SaveAuthorInteractor;
import com.nst.domain.author.single.GetAuthorInteractor;
import com.nst.domain.book.Book;
import com.nst.domain.book.all.GetAllBooksInteractor;
import com.nst.domain.book.popular.GetPopularBooksInteractor;
import com.nst.domain.category.Category;
import com.nst.domain.category.popular.GetPopularCategoriesInteractor;
import com.nst.domain.user.login.GetLoggedInUserInteractor;
import com.nst.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AuthorController {

	@Autowired private GetAuthorInteractor getAuthorInteractor;
	@Autowired private SaveAuthorInteractor saveAuthorInteractor;
	@Autowired private DeleteAuthorInteractor deleteAuthorInteractor;
	@Autowired private GetAllAuthorsInteractor getAllAuthorsInteractor;
	@Autowired private EditAuthorInteractor editAuthorInteractor;
	@Autowired private GetAllBooksInteractor getAllBooksInteractor;
	@Autowired private GetPopularBooksInteractor getPopularBooksInteractor;
	@Autowired private GetPopularCategoriesInteractor getPopularCategoriesInteractor;
	@Autowired private GetLoggedInUserInteractor getLoggedInUserInteractor;

	@Value("${image.location}") public String imageLocation;

	@RequestMapping(value="add_author", method = RequestMethod.GET)
	public String addAuthor(Model model){
		model.addAttribute(new AuthorDb());
		return "add_author";
	}
	
	@RequestMapping(value="action/add_author", method = RequestMethod.POST)
	public String addAuthor(@Valid @ModelAttribute AuthorDb author, BindingResult result, MultipartFile file, Model model) throws Exception {
		
		if(result.hasErrors()){
			 List<FieldError> errors = result.getFieldErrors();
			    for (FieldError error : errors ) {
			        System.out.println (error.getObjectName() + " - " + error.getDefaultMessage());
			    }
			return "add_author";
		}
		
		AuthorDb authorForSave = handleAuthor(author, file);

		Author savedAuthor = saveAuthorInteractor.execute(AuthorMapper.convertToPlainModel(authorForSave));
		AuthorDb savedDbAuthor = AuthorMapper.convertToDbModel(savedAuthor);
		
		if(savedDbAuthor == null){
			return "add_author";
		}	
				
		model.addAttribute(savedDbAuthor);
		model.addAttribute("book", new BookDb());
		model.addAllAttributes(getAllBooks());
		
		return "redirect:/index";
	}	
		
	@RequestMapping(value ="/action/deleteAuthor", method = RequestMethod.GET)
	public String deleteAuthor(Model model, int id){
		Author deleteAuthor = getAuthorInteractor.execute(id);
		deleteAuthorInteractor.execute(deleteAuthor);
		return "redirect:/index";		
	}
	
	@RequestMapping(value ="/editAuthor", method = RequestMethod.GET)
	public String editAuthor(Model model, int id){
		Author editAuthor = editAuthorInteractor.execute(id);
		model.addAttribute("author", AuthorMapper.convertToDbModel(editAuthor));
		return "edit_author";		
	}

	@RequestMapping(value ="author", method = RequestMethod.GET)
	public String author(Model model, long id) throws Exception {
		Author author = getAuthorInteractor.execute((int) id);
		model.addAttribute("author", author);
		model.addAttribute("authorBooks", booksForAuthor(AuthorMapper.convertToDbModel(author)));
		model.addAllAttributes(getPopularBooks());
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());		
		return "author";
	}
	
	@RequestMapping(value = ("/authors"), method = RequestMethod.GET)
	public String authors(Model model) throws Exception {
		model.addAttribute("authors", getAllAuthors());
		model.addAttribute("book", new BookDb());
		model.addAttribute("popularCategories", getPopularCategories());
		return "all_authors";
	}
	
	@ModelAttribute("popularBooks")
	public List<BookDb> getPopularBooks() {
		List<Book> books = getPopularBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	@ModelAttribute("allAuthors")
	public List<AuthorDb> getAllAuthors() {
		List<Author> authors = getAllAuthorsInteractor.execute(null);
		return AuthorMapper.convertToListOfDbModels(authors);
	}
	
	@ModelAttribute("books")
	public List<BookDb> getAllBooks() {
		List<Book> books = getAllBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	@ModelAttribute("popularCategories")
	public List<CategoryDb> getPopularCategories() throws Exception {
		List<Category> categories = getPopularCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categories);
	}	

	private AuthorDb handleAuthor(AuthorDb author, MultipartFile file) throws Exception {
		addImageToAuthor(author, file);	
		User currentUser = getLoggedInUserInteractor.execute(true);
		author.setUser(UserMapper.convertToDbModel(currentUser));
		return author;
	}

	private void addImageToAuthor(AuthorDb author, MultipartFile file) {
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(imageLocation, file.getOriginalFilename()));
				author.setImage(file.getOriginalFilename());
				System.out.println("LOKACIJA: "+ imageLocation +"/"+file.getOriginalFilename());
			} catch (IOException|RuntimeException e) {
				System.out.println(e.getMessage());
			}
		} 		
	}	

	private List<BookDb> booksForAuthor(AuthorDb author) {
		//Find books for this author
		List<BookDb> books = new ArrayList<>();
		for(BookDb b : author.getBookList()){
			books.add(b);
		}
		return books;
	}	
}
