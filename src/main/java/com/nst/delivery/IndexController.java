package com.nst.delivery;

import java.util.List;

import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryMapper;
import com.nst.data.user.UserMapper;
import com.nst.domain.book.Book;
import com.nst.domain.book.all.GetAllBooksInteractor;
import com.nst.domain.book.popular.GetPopularBooksInteractor;
import com.nst.domain.category.Category;
import com.nst.domain.category.popular.GetPopularCategoriesInteractor;
import com.nst.domain.user.login.GetLoggedInUserInteractor;
import com.nst.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nst.data.category.CategoryDb;
import com.nst.data.user.UserDb;


@Controller
public class IndexController {

	@Autowired GetAllBooksInteractor getAllBooksInteractor;
	@Autowired GetPopularBooksInteractor getPopularBooksInteractor;
	@Autowired GetPopularCategoriesInteractor getPopularCategoriesInteractor;
	@Autowired GetLoggedInUserInteractor getLoggedInUserInteractor;
	
	@RequestMapping(value = "/")
	public String index(Model model) throws Exception {
		return getModelForIndexPage(model);
	}

	@RequestMapping(value = "/index")
	public String showIndex(Model model) throws Exception {
		return getModelForIndexPage(model);
	}

	private String getModelForIndexPage(Model model) throws Exception {
		model.addAttribute("book", new BookDb());
		model.addAttribute("books", displayAllBooks());
		model.addAttribute("popularBooks", getPopularBooks());
		model.addAttribute("popularCategories", getPopularCategories());
		return "index";
	}
	
	@RequestMapping(value="/admin")
	public String login(Model model){
		model.addAttribute(new BookDb());
		return "admin";
	}
	
	
	@ModelAttribute("books")
	public List<BookDb> displayAllBooks() {
		List<Book> books = getAllBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	

	@ModelAttribute("popularBooks")
	public List<BookDb> getPopularBooks() {
		List<Book> books = getPopularBooksInteractor.execute(null);
		return BookMapper.convertToListOfDbModels(books);
	}
	
	@ModelAttribute("popularCategories")
	public List<CategoryDb> getPopularCategories() throws Exception {
		List<Category> categories = getPopularCategoriesInteractor.execute(null);
		return CategoryMapper.convertToListOfDbModels(categories);
	}
	
	@ModelAttribute("user")
	public UserDb getLoggedIn() throws Exception {
		User user = getLoggedInUserInteractor.execute(false);
		return UserMapper.convertToDbModel(user);
	}
}
