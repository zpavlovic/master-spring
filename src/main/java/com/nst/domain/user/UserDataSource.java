package com.nst.domain.user;

public interface UserDataSource {
    User getLoggedInUser(Boolean forceRefresh);
}
