package com.nst.domain.user.login;

import com.nst.domain.UseCase;
import com.nst.domain.user.User;
import com.nst.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetLoggedInUserInteractor extends UseCase<User, Boolean> {

    @Autowired
    private UserRepository repository;

    @Override
    public User execute(Boolean forceRefresh) throws Exception {
        return repository.getLoggedInUser(forceRefresh);
    }
}
