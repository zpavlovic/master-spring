package com.nst.domain.user;

public interface UserRepository {
    User getLoggedInUser(Boolean forceRefresh);
}
