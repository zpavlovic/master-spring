package com.nst.domain.category.get;

import com.nst.domain.UseCase;
import com.nst.domain.category.Category;
import com.nst.domain.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetCategoryInteractor extends UseCase<Category, Integer> {

    @Autowired
    private CategoryRepository repository;

    @Override
    public Category execute(Integer id) throws Exception {
        return repository.findById(id);
    }
}
