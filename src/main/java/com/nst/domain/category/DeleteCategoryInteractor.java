package com.nst.domain.category;

import com.nst.domain.UseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteCategoryInteractor extends UseCase<Void, Integer> {

    @Autowired
    private CategoryRepository repository;

    @Override
    public Void execute(Integer id) throws Exception {
        Category category = repository.findById(id);
        repository.delete(category);
        return null;
    }
}
