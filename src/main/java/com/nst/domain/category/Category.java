package com.nst.domain.category;

import com.nst.domain.user.User;

public class Category {

    private long id;
    private String name;
    private String description;
    private User user;

    public Category(long id, String name, String description, User user) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.user = user;
    }

    public Category() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public User getUser() {
        return user;
    }
}
