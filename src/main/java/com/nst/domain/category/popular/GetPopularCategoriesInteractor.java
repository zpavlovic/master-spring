package com.nst.domain.category.popular;

import com.nst.domain.UseCase;
import com.nst.domain.category.Category;
import com.nst.domain.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetPopularCategoriesInteractor extends UseCase<List<Category>, Void>{

    @Autowired
    private CategoryRepository repository;
    @Override
    public List<Category> execute(Void p) throws Exception {
        return repository.getPopularCategories();
    }
}
