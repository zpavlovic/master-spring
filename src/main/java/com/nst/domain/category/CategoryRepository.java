package com.nst.domain.category;

import java.util.List;

public interface CategoryRepository {
    List<Category> findAll();

    List<Category> getPopularCategories();

    Category save(Category category);

    Category findById(Integer id);

    void delete(Category category);
}
