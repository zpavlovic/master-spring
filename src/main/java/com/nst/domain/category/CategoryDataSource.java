package com.nst.domain.category;

import java.util.List;

public interface CategoryDataSource {
    List<Category> findAll();

    List<Category> getPopularCategories();

    Category save(Category category);

    Category findById(Integer id);

    void delete(Category category);
}
