package com.nst.domain.category.save;

import com.nst.domain.UseCase;
import com.nst.domain.category.Category;
import com.nst.domain.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SaveCategoryInteractor extends UseCase<Category, Category>{

    @Autowired
    private CategoryRepository repository;

    @Override
    public Category execute(Category category) throws Exception {
        return repository.save(category);
    }
}
