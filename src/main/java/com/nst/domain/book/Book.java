package com.nst.domain.book;

import com.nst.domain.author.Author;
import com.nst.domain.category.Category;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.user.User;

import java.sql.Date;
import java.util.List;

public class Book {

    private long id;
    private String name;
    private String description;
    private String image;
    private Date publishedDate;
    private Date addedDate;
    private int pagesNumber;
    private long views;
    private String shortDescription;
    private int isbn10;
    private int isbn13;
    private String downloadLink;
    private User user;
    private List<Author> authorsList;
    private List<Category> categoryList;
    private Publisher publisher;

    public Book(long id, String name, String description, String image, Date publishedDate, Date addedDate,
                int pagesNumber, long views, String shortDescription, int isbn10, int isbn13, String downloadLink,
                User user, List<Author> authorsList, List<Category> categoryList, Publisher publisher) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.publishedDate = publishedDate;
        this.addedDate = addedDate;
        this.pagesNumber = pagesNumber;
        this.views = views;
        this.shortDescription = shortDescription;
        this.isbn10 = isbn10;
        this.isbn13 = isbn13;
        this.downloadLink = downloadLink;
        this.user = user;
        this.authorsList = authorsList;
        this.categoryList = categoryList;
        this.publisher = publisher;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public int getPagesNumber() {
        return pagesNumber;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views){
        this.views = views;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public int getIsbn10() {
        return isbn10;
    }

    public int getIsbn13() {
        return isbn13;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public User getUser() {
        return user;
    }

    public List<Author> getAuthorsList() {
        return authorsList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public void setPagesNumber(int pagesNumber) {
        this.pagesNumber = pagesNumber;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public void setIsbn10(int isbn10) {
        this.isbn10 = isbn10;
    }

    public void setIsbn13(int isbn13) {
        this.isbn13 = isbn13;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAuthorsList(List<Author> authorsList) {
        this.authorsList = authorsList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
}
