package com.nst.domain.book.search;

import com.nst.domain.UseCase;
import com.nst.domain.book.Book;
import com.nst.domain.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SearchBookInteractor extends UseCase<List<Book>, String>{

    @Autowired
    private BookRepository repository;

    @Override
    public List<Book> execute(String name) throws Exception {
        return repository.search(name);
    }
}
