package com.nst.domain.book.recommend;

import com.nst.domain.UseCase;
import com.nst.domain.book.Book;
import com.nst.domain.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RecommendBooksInteractor extends UseCase<List<Book>, Void> {

    @Autowired
    private BookRepository repository;

    @Override
    public List<Book> execute(Void p) {
        return repository.recommendBooks();
    }
}
