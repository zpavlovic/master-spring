package com.nst.domain.book.save;

import com.nst.domain.UseCase;
import com.nst.domain.book.Book;
import com.nst.domain.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SaveBookInteractor extends UseCase<Book, Book> {

    @Autowired
    private BookRepository repository;

    @Override
    public Book execute(Book book) {
        return repository.save(book);
    }
}
