package com.nst.domain.book;

import com.nst.domain.UseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteBookInteractor extends UseCase<Void, Integer> {

    @Autowired
    private BookRepository repository;

    @Override
    public Void execute(Integer integer) throws Exception {
        Book book = repository.findById(integer);
        repository.delete(book);
        return null;
    }
}
