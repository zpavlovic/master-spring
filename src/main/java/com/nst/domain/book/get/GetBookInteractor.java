package com.nst.domain.book.get;

import com.nst.domain.UseCase;
import com.nst.domain.book.Book;
import com.nst.domain.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetBookInteractor extends UseCase<Book, Integer> {

    @Autowired
    private BookRepository repository;

    @Override
    public Book execute(Integer id) {
        return repository.findById(id);
    }
}
