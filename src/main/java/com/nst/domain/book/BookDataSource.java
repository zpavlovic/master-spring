package com.nst.domain.book;

import java.util.List;

public interface BookDataSource {

    Book save(Book book);

    List<Book> getPopularBooks();

    List<Book> findAll();

    Book findById(int id);

    List<Book> recommendBooks();

    List<Book> search(String name) throws Exception;

    void delete(Book book);
}
