package com.nst.domain.author.all;

import com.nst.domain.UseCase;
import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAllAuthorsInteractor extends UseCase<List<Author>, Void> {

    @Autowired
    private AuthorRepository repository;

    @Override
    public List<Author> execute(Void unused) {
        return repository.findAll();
    }
}
