package com.nst.domain.author.save;

import com.nst.domain.UseCase;
import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SaveAuthorInteractor extends UseCase<Author, Author> {

    @Autowired
    private AuthorRepository repository;

    @Override
    public Author execute(Author author) {
        return repository.save(author);
    }
}
