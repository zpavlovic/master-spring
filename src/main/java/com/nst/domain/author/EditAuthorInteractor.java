package com.nst.domain.author;

import com.nst.domain.UseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditAuthorInteractor extends UseCase<Author, Integer> {

    @Autowired
    private AuthorRepository repository;

    @Override
    public Author execute(Integer id) {
        Author author = repository.findById(id);
        return repository.save(author);
    }
}
