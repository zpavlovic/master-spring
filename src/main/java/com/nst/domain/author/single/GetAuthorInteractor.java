package com.nst.domain.author.single;

import com.nst.domain.UseCase;
import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetAuthorInteractor  extends UseCase<Author, Integer>{

    @Autowired
    private AuthorRepository repository;

    @Override
    public Author execute(Integer id) {
        return repository.findById(id);
    }
}
