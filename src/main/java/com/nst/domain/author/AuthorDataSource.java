package com.nst.domain.author;

import java.util.List;

public interface AuthorDataSource {

    Author save(Author author);

    void delete(Author author);

    List<Author> findAll();

    Author findById(int id);
}
