package com.nst.domain.author.delete;

import com.nst.domain.UseCase;
import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteAuthorInteractor extends UseCase<Void, Author> {

    @Autowired
    private AuthorRepository repository;

    @Override
    public Void execute(Author author) {
        repository.delete(author);
        return null;
    }
}
