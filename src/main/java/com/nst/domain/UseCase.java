package com.nst.domain;

public abstract class UseCase<ReturnType, Param> {
    public abstract ReturnType execute(Param p) throws Exception;
}
