package com.nst.domain.publisher;

import java.util.List;

public interface PublisherDataSource {
    List<Publisher> findAll();

    Publisher save(Publisher publisher);

    Publisher findById(Integer id);

    void delete(Publisher publisher);
}
