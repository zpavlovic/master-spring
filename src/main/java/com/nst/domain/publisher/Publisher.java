package com.nst.domain.publisher;

import com.nst.domain.user.User;

public class Publisher {

    private long id;
    private String name;
    private String website;
    private User user;

    public Publisher(long id, String name, String website, User user) {
        super();
        this.id = id;
        this.name = name;
        this.website = website;
        this.user = user;
    }

    public Publisher() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
