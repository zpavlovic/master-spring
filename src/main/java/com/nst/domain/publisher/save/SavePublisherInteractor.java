package com.nst.domain.publisher.save;

import com.nst.domain.UseCase;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SavePublisherInteractor extends UseCase<Publisher, Publisher>{

    @Autowired
    private PublisherRepository repository;

    @Override
    public Publisher execute(Publisher publisher) throws Exception {
        return repository.save(publisher);
    }
}
