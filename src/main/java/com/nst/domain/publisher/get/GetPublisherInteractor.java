package com.nst.domain.publisher.get;

import com.nst.domain.UseCase;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetPublisherInteractor extends UseCase<Publisher, Integer> {

    @Autowired
    private PublisherRepository repository;

    @Override
    public Publisher execute(Integer id) throws Exception {
        return repository.findById(id);
    }
}
