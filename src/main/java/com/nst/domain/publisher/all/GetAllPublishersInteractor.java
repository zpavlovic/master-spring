package com.nst.domain.publisher.all;

import com.nst.domain.UseCase;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAllPublishersInteractor extends UseCase<List<Publisher>, Void> {

    @Autowired
    private PublisherRepository repository;

    @Override
    public List<Publisher> execute(Void p) throws Exception {
        return repository.findAll();
    }
}
