package com.nst.domain.publisher.delete;

import com.nst.domain.UseCase;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeletePublisherInteractor extends UseCase<Void, Integer> {

    @Autowired
    private PublisherRepository repository;

    @Override
    public Void execute(Integer id) throws Exception {
        Publisher publisher = repository.findById(id);
        repository.delete(publisher);
        return null;
    }
}
