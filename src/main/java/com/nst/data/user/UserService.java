package com.nst.data.user;

import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {
	
	UserDb findByUsername(String username);
	
	UserDb register(UserDb user);
	
	void autoLogin(UserDb user);
	
	UserDb getLoggedInUser(Boolean forceFresh);

	UserDb save(UserDb user);
	
}
