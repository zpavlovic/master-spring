package com.nst.data.user;

import com.nst.domain.user.User;
import com.nst.domain.user.UserDataSource;
import com.nst.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDbRepository implements UserRepository{

    @Autowired
    private UserDataSource dataSource;

    @Override
    public User getLoggedInUser(Boolean forceRefresh) {
        return dataSource.getLoggedInUser(forceRefresh);
    }
}
