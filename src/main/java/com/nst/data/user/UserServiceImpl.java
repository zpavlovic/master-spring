package com.nst.data.user;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userRepository;

	@Autowired
	private HttpSession httpSession;

	public final String CURRENT_USER_KEY = "CURRENT_USER";

	@Override
	public UserDb findByUsername(String username) {
		UserDb u = userRepository.findByUsername(username);
		return u;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDb user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return user;
	}

	@Override
	public UserDb register(UserDb user) {
		user.setPassword(encodeUserPassword(user.getPassword()));

		if (this.userRepository.findByUsername(user.getUsername()) == null) {
			this.userRepository.save(user);
			return user;
		}

		return null;
	}


	@Override
	public void autoLogin(UserDb user) {
		autoLogin(user.getUsername());
	}

	public void autoLogin(String username) {
		UserDetails userDetails = this.loadUserByUsername(username);
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken (userDetails, null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		if(auth.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
	}

	public String encodeUserPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	@Override
	public UserDb getLoggedInUser(Boolean forceFresh) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		UserDb user = (UserDb) httpSession.getAttribute(CURRENT_USER_KEY);
		if(forceFresh || httpSession.getAttribute(CURRENT_USER_KEY) == null) {
			user = this.userRepository.findByUsername(userName);
			httpSession.setAttribute(CURRENT_USER_KEY, user);
		}
		return user;
	}

	@Override
	public UserDb save(UserDb user) {
		return userRepository.save(user);
	}
}
