package com.nst.data.user;

import com.nst.domain.user.User;
import com.nst.domain.user.UserDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDbDataSource implements UserDataSource{

    @Autowired
    private UserService service;

    @Override
    public User getLoggedInUser(Boolean forceRefresh) {
        UserDb userDb = service.getLoggedInUser(forceRefresh);
        return UserMapper.convertToPlainModel(userDb);
    }
}
