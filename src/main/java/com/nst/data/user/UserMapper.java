package com.nst.data.user;

import com.nst.domain.user.User;

public class UserMapper {

    public static User convertToPlainModel(UserDb userDb){
       return new User(userDb.getId(), userDb.getUsername(), userDb.getPassword(), userDb.getFirstName(), userDb.getLastName());
    }

    public static UserDb convertToDbModel(User user){
        return new UserDb(user.getId(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName());
    }
}
