package com.nst.data.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends JpaRepository<UserDb, Long> {
	
	UserDb findByUsername(String username);

}
