package com.nst.data.author;

import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthorDbDataSource implements AuthorDataSource {

    @Autowired
    private AuthorService service;

    @Override
    public Author save(Author author) {
        AuthorDb savedAuthor = service.save(AuthorMapper.convertToDbModel(author));
        return AuthorMapper.convertToPlainModel(savedAuthor);
    }

    @Override
    public void delete(Author author) {
        service.delete(AuthorMapper.convertToDbModel(author));
    }

    @Override
    public List<Author> findAll() {
        List<AuthorDb> authorDbs = service.findAll();
        return AuthorMapper.convertToListOfPlainModels(authorDbs);
    }

    @Override
    public Author findById(int id) {
        AuthorDb authorDb = service.findById(id);
        return AuthorMapper.convertToPlainModel(authorDb);
    }
}
