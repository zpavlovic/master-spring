package com.nst.data.author;

import java.util.List;

public interface AuthorService {
	
	AuthorDb save(AuthorDb author);

	void delete(AuthorDb author);

	List<AuthorDb> findAll();

	AuthorDb findById(long id);

}