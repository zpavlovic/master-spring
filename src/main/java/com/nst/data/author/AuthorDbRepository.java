package com.nst.data.author;

import com.nst.domain.author.Author;
import com.nst.domain.author.AuthorDataSource;
import com.nst.domain.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthorDbRepository implements AuthorRepository{

    @Autowired
    private AuthorDataSource dataSource;

    @Override
    public Author save(Author author) {
        return dataSource.save(author);
    }

    @Override
    public void delete(Author author) {
        dataSource.delete(author);
    }

    @Override
    public List<Author> findAll() {
        return dataSource.findAll();
    }

    @Override
    public Author findById(int id) {
        return dataSource.findById(id);
    }
}
