package com.nst.data.author;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
	
	@Autowired
	private AuthorDao authorRepository;

	@Override
	public AuthorDb save(AuthorDb author) {
		return authorRepository.save(author);
	}


	@Override
	public void delete(AuthorDb author) {
		authorRepository.delete(author);		
	}

	@Override
	public List<AuthorDb> findAll() {
		return authorRepository.findAll();
	}


	@Override
	public AuthorDb findById(long id) {
		return authorRepository.findById(id);
	}

}
