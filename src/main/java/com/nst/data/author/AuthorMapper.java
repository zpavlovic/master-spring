package com.nst.data.author;

import com.nst.data.user.UserMapper;
import com.nst.domain.author.Author;

import java.util.ArrayList;
import java.util.List;

public class AuthorMapper {

    public static Author convertToPlainModel(AuthorDb authorDb){
        return new Author(authorDb.getId(), authorDb.getFirstName(), authorDb.getLastName(), authorDb.getDescription(),
                authorDb.getImage(), authorDb.getWebsite(), UserMapper.convertToPlainModel(authorDb.getUser()));
    }

    public static AuthorDb convertToDbModel(Author author){
        return new AuthorDb(author.getId(), author.getFirstName(), author.getLastName(), author.getDescription(),
                author.getImage(), author.getWebsite(), UserMapper.convertToDbModel(author.getUser()));
    }

    public static List<Author> convertToListOfPlainModels(List<AuthorDb> authorDbs){
        List<Author> authors = new ArrayList<>();
        for(AuthorDb authorDb : authorDbs){
            authors.add(AuthorMapper.convertToPlainModel(authorDb));
        }
        return authors;
    }

    public static List<AuthorDb> convertToListOfDbModels(List<Author> authors) {
        List<AuthorDb> authorsDb = new ArrayList<>();
        for(Author author : authors){
            authorsDb.add(AuthorMapper.convertToDbModel(author));
        }
        return authorsDb;
    }
}
