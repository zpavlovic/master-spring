package com.nst.data.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorDao extends JpaRepository<AuthorDb, Long> {
	
	@Query("SELECT a FROM AuthorDb a WHERE a.id = ?1")
	AuthorDb findById(long id);

}
