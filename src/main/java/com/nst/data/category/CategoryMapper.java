package com.nst.data.category;

import com.nst.data.user.UserMapper;
import com.nst.domain.category.Category;
import com.nst.domain.user.User;

import java.util.ArrayList;
import java.util.List;

public class CategoryMapper {

    public static Category convertToPlainModel(CategoryDb categoryDb) {
        return new Category(categoryDb.getId(), categoryDb.getName(), categoryDb.getDescription(),
                UserMapper.convertToPlainModel(categoryDb.getUser()));
    }

    public static CategoryDb convertToDbModel(Category category) {
        return new CategoryDb(category.getId(), category.getName(), category.getDescription(),
                UserMapper.convertToDbModel(category.getUser()));
    }

    public static List<CategoryDb> convertToListOfDbModels(List<Category> categoryList) {
        List<CategoryDb> categoryDbs = new ArrayList<>();
        for(Category category : categoryList){
            categoryDbs.add(convertToDbModel(category));
        }
        return categoryDbs;
    }

    public static List<Category> convertToListOfPlainModels(List<CategoryDb> categoriesDb) {
        List<Category> categories = new ArrayList<>();
        for(CategoryDb categoryDb : categoriesDb){
            categories.add(convertToPlainModel(categoryDb));
        }
        return categories;
    }
}
