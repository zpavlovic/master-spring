package com.nst.data.category;

import java.util.List;

public interface CategoryService {
	
	CategoryDb save(CategoryDb category);

	void delete(CategoryDb category);

	CategoryDb findOne(CategoryDb category);

	List<CategoryDb> findAll();

	CategoryDb findById(long id);

	List<CategoryDb> find5Categories();

}
