package com.nst.data.category;

import com.nst.domain.category.Category;
import com.nst.domain.category.CategoryDataSource;
import com.nst.domain.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryDbRepository implements CategoryRepository {

    @Autowired
    private CategoryDataSource dataSource;

    @Override
    public List<Category> findAll() {
        return dataSource.findAll();
    }

    @Override
    public List<Category> getPopularCategories() {
        return dataSource.getPopularCategories();
    }

    @Override
    public Category save(Category category) {
        return dataSource.save(category);
    }

    @Override
    public void delete(Category category) {
        dataSource.delete(category);
    }

    @Override
    public Category findById(Integer id) {
        return dataSource.findById(id);
    }
}
