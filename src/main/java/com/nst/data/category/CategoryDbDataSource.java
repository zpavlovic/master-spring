package com.nst.data.category;

import com.nst.domain.category.Category;
import com.nst.domain.category.CategoryDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryDbDataSource implements CategoryDataSource{

    @Autowired
    private CategoryService service;

    @Override
    public List<Category> findAll() {
        List<CategoryDb> categoryDbs = service.findAll();
        return CategoryMapper.convertToListOfPlainModels(categoryDbs);
    }

    @Override
    public List<Category> getPopularCategories() {
        List<CategoryDb> categoryDbs = service.find5Categories();
        return CategoryMapper.convertToListOfPlainModels(categoryDbs);
    }

    @Override
    public Category save(Category category) {
        CategoryDb categoryDb = service.save(CategoryMapper.convertToDbModel(category));
        return CategoryMapper.convertToPlainModel(categoryDb);
    }

    @Override
    public Category findById(Integer id) {
        CategoryDb categoryDb = service.findById(id);
        return CategoryMapper.convertToPlainModel(categoryDb);
    }

    @Override
    public void delete(Category category) {
        service.delete(CategoryMapper.convertToDbModel(category));
    }
}
