package com.nst.data.category;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryDao extends JpaRepository<CategoryDb, Long> {

	
	@Query("SELECT c FROM CategoryDb c WHERE c.id = ?1")
    CategoryDb findById(long id);
	
	@Query("SELECT c FROM CategoryDb c")
	List<CategoryDb> find5Categories(Pageable pageable);

}
