package com.nst.data.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryDao categoryRepository;

	@Override
	public CategoryDb save(CategoryDb category) {
		CategoryDb c = categoryRepository.save(category);
		return c;
	}

	@Override
	public void delete(CategoryDb category) {
		categoryRepository.delete(category);		
	}

	@Override
	public CategoryDb findOne(CategoryDb category) {
		return categoryRepository.findOne(category.getId());
	}

	@Override
	public List<CategoryDb> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public CategoryDb findById(long id) {
		return categoryRepository.findById(id);
	}

	@Override
	public List<CategoryDb> find5Categories() {
		return categoryRepository.find5Categories(new PageRequest(0, 10));
	}

}
