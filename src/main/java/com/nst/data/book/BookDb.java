package com.nst.data.book;

import com.nst.data.author.AuthorDb;
import com.nst.data.category.CategoryDb;
import com.nst.data.publisher.PublisherDb;
import com.nst.data.user.UserDb;
import org.hibernate.annotations.Cascade;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "book")
public class BookDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column
    private long id;

    @Size(min = 1, message = "Name can not be empty")
    @NotNull
    @Column
    private String name;

    @Size(min = 1, message = "Description can not be empty")
    @NotNull
    @Column
    private String description;

    @Column
    private String image;


    @Column(name = "published_date")
    private Date publishedDate;

    @Column(name = "added_date")
    private Date addedDate;

    @Column
    private int pagesNumber;

    @Column
    private long views = 0;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "isbn_10")
    private int isbn10 = 0;

    @Column(name = "isbn_13")
    private int isbn13 = 0;

    @Size(min = 1, message = "Link can not be empty")
    @NotNull
    @Column(name = "link")
    private String downloadLink;


    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private UserDb user;

    @JoinColumn(name = "publisher_id", referencedColumnName = "id")
    @ManyToOne()
    @NotNull
    private PublisherDb publisher;

    //AuthorDb
    @JoinTable(name = "book_author", joinColumns = {
            @JoinColumn(name = "book_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "author_id", referencedColumnName = "id")})
    @ManyToMany
    private List<AuthorDb> authorsList;

    //CategoryDb
    @JoinTable(name = "book_category", joinColumns = {
            @JoinColumn(name = "book_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "category_id", referencedColumnName = "id")})
    @ManyToMany
    @NotNull
    private List<CategoryDb> categoryList;

    public BookDb() {
        // TODO Auto-generated constructor stub
    }

    public BookDb(long id, String name, String description, String image, Date publishedDate, Date addedDate,
                  int pagesNumber, long views, String shortDescription, int isbn10, int isbn13, String downloadLink,
                  UserDb user, List<AuthorDb> authorsList, List<CategoryDb> categoryList, PublisherDb publisher) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.publishedDate = publishedDate;
        this.addedDate = addedDate;
        this.pagesNumber = pagesNumber;
        this.views = views;
        this.shortDescription = shortDescription;
        this.isbn10 = isbn10;
        this.isbn13 = isbn13;
        this.downloadLink = downloadLink;
        this.user = user;
        this.authorsList = authorsList;
        this.categoryList = categoryList;
        this.publisher = publisher;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public int getPagesNumber() {
        return pagesNumber;
    }

    public void setPagesNumber(int pagesNumber) {
        this.pagesNumber = pagesNumber;
    }

    public UserDb getUser() {
        return user;
    }

    public void setUser(UserDb user) {
        this.user = user;
    }

    public List<CategoryDb> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryDb> categoryList) {
        this.categoryList = categoryList;
    }

    public List<AuthorDb> getAuthorsList() {
        return authorsList;
    }

    public void setAuthorsList(List<AuthorDb> authorsList) {
        this.authorsList = authorsList;
    }

    public PublisherDb getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDb publisher) {
        this.publisher = publisher;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getShortDescription() {
        return shortDescription;
    }


    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }


    public int getIsbn10() {
        return isbn10;
    }


    public void setIsbn10(int isbn10) {
        this.isbn10 = isbn10;
    }


    public int getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(int isbn13) {
        this.isbn13 = isbn13;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BookDb)) {
            return false;
        }
        BookDb other = (BookDb) object;
        if (other.id != this.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

}
