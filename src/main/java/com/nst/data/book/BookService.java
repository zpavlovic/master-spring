package com.nst.data.book;

import java.util.List;

public interface BookService {
	
	BookDb saveBook(BookDb book);

	void delete(BookDb book);

	BookDb findOne(BookDb book);
	
	List<BookDb> findAll();

	List<BookDb> search(String value) throws Exception;

	BookDb findById(long id);

	List<BookDb> getPopularBooks();
	
	List<BookDb> reccommendBooks();

}
