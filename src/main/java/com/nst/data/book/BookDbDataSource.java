package com.nst.data.book;

import com.nst.domain.book.Book;
import com.nst.domain.book.BookDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookDbDataSource implements BookDataSource {

    @Autowired
    private BookService service;

    @Override
    public Book save(Book book) {
        BookDb bookDb = service.saveBook(BookMapper.convertToDbModel(book));
        return BookMapper.convertToPlainModel(bookDb);
    }

    @Override
    public List<Book> getPopularBooks() {
        List<BookDb> bookDbs = service.getPopularBooks();
        return BookMapper.convertListOfPlainBooks(bookDbs);
    }

    @Override
    public List<Book> findAll() {
        List<BookDb> bookDbs = service.findAll();
        return BookMapper.convertListOfPlainBooks(bookDbs);
    }

    @Override
    public Book findById(int id) {
        BookDb bookDb = service.findById(id);
        return BookMapper.convertToPlainModel(bookDb);
    }

    @Override
    public List<Book> recommendBooks() {
        List<BookDb> bookDbs = service.reccommendBooks();
        return BookMapper.convertListOfPlainBooks(bookDbs);
    }

    @Override
    public List<Book> search(String name) throws Exception {
        List<BookDb> bookDbs = service.search(name);
        return BookMapper.convertListOfPlainBooks(bookDbs);
    }

    @Override
    public void delete(Book book) {
        service.delete(BookMapper.convertToDbModel(book));
    }

}
