package com.nst.data.book;

import com.nst.data.author.AuthorMapper;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryMapper;
import com.nst.data.publisher.PublisherDb;
import com.nst.data.publisher.PublisherMapper;
import com.nst.data.user.UserDb;
import com.nst.data.user.UserMapper;
import com.nst.domain.book.Book;
import com.nst.domain.category.Category;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.user.User;

import java.util.ArrayList;
import java.util.List;

public class BookMapper {

    public static BookDb convertToDbModel(Book book) {
        return new BookDb(book.getId(), book.getName(), book.getDescription(), book.getImage(), book.getPublishedDate(), book.getAddedDate(),
                book.getPagesNumber(), book.getViews(), book.getShortDescription(), book.getIsbn10(), book.getIsbn13(),
                book.getDownloadLink(), UserMapper.convertToDbModel(book.getUser()), AuthorMapper.convertToListOfDbModels(book.getAuthorsList()),
                CategoryMapper.convertToListOfDbModels(book.getCategoryList()), PublisherMapper.convertToDbModel(book.getPublisher()));
    }

    public static Book convertToPlainModel(BookDb book){
        return new Book(book.getId(), book.getName(), book.getDescription(), book.getImage(), book.getPublishedDate(), book.getAddedDate(),
                book.getPagesNumber(), book.getViews(), book.getShortDescription(), book.getIsbn10(), book.getIsbn13(),
                book.getDownloadLink(), UserMapper.convertToPlainModel(book.getUser()), AuthorMapper.convertToListOfPlainModels(book.getAuthorsList()),
                CategoryMapper.convertToListOfPlainModels(book.getCategoryList()), PublisherMapper.convertToPlainModel(book.getPublisher()));
    }

    public static List<Book> convertListOfPlainBooks(List<BookDb> bookDbs) {
        List<Book> books = new ArrayList<>();
        for(BookDb bookDb : bookDbs){
            books.add(BookMapper.convertToPlainModel(bookDb));
        }
        return books;
    }

    public static List<BookDb> convertToListOfDbModels(List<Book> books) {
        List<BookDb> bookDbs = new ArrayList<>();
        for(Book book : books){
            bookDbs.add(BookMapper.convertToDbModel(book));
        }
        return bookDbs;
    }
}
