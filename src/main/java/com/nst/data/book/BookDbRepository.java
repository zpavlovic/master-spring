package com.nst.data.book;

import com.nst.domain.book.Book;
import com.nst.domain.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookDbRepository implements BookRepository {

    @Autowired
    private BookDbDataSource dataSource;

    @Override
    public Book save(Book book) {
        return dataSource.save(book);
    }

    @Override
    public List<Book> getPopularBooks() {
        return dataSource.getPopularBooks();
    }

    @Override
    public List<Book> findAll() {
        return dataSource.findAll();
    }

    @Override
    public Book findById(int id) {
        return dataSource.findById(id);
    }

    @Override
    public List<Book> recommendBooks() {
        return dataSource.recommendBooks();
    }

    @Override
    public List<Book> search(String name) throws Exception {
        return dataSource.search(name);
    }

    @Override
    public void delete(Book book) {
        dataSource.delete(book);
    }
}
