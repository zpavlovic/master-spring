package com.nst.data.book;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javassist.NotFoundException;

@Service
@Transactional
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookDao bookRepository;

	@Override
	public BookDb saveBook(BookDb book) {
		BookDb saveBook = bookRepository.save(book);
		return saveBook;
	}

	@Override
	public void delete(BookDb book) {
		bookRepository.delete(book);
		
	}

	@Override
	public BookDb findOne(BookDb book) {
		return bookRepository.findOne(book.getId());
	}

	@Override
	public List<BookDb> findAll() {
		return bookRepository.findAll();
	}

	@Override
	public List<BookDb> search(String value) throws Exception {
		
		List<BookDb> books = bookRepository.findByNameContaining(value);
		
		if(books == null){
			throw new NotFoundException("BookDb not found");
		}
		
		if(books.isEmpty()){
			throw new Exception("BookDb not found");
		}
		
		return books;
	}

	@Override
	public BookDb findById(long id) {
		return bookRepository.findById(id);
	}

	@Override
	public List<BookDb> getPopularBooks() {
		return bookRepository.getPopularBooks(new PageRequest(0,5));
	}

	@Override
	public List<BookDb> reccommendBooks() {
		return bookRepository.reccommendBooks(new PageRequest(0, 3));
	}

}
