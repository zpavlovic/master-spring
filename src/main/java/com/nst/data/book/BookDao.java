package com.nst.data.book;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookDao extends JpaRepository<BookDb, Long> {
	
	List<BookDb> findByNameContaining(String value);
	
	@Query("SELECT b FROM BookDb b WHERE b.id = ?1")
	BookDb findById(long id);
	
	@Query("SELECT b FROM BookDb b ORDER BY b.views DESC")
	List<BookDb> getPopularBooks(Pageable pagable);
	
	
	@Query("SELECT b FROM BookDb b")
	List<BookDb> reccommendBooks(Pageable pagable);
	
	@Query("SELECT b FROM BookDb b ORDER BY b.id DESC")
	List<BookDb> findAll();
	
}
