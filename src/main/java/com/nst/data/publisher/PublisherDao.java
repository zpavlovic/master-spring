package com.nst.data.publisher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherDao extends JpaRepository<PublisherDb, Long> {
	
	@Query("SELECT p FROM PublisherDb p WHERE p.id = ?1")
    PublisherDb findById(long id);

}
