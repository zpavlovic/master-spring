package com.nst.data.publisher;

import com.nst.data.user.UserMapper;
import com.nst.domain.publisher.Publisher;

import java.util.ArrayList;
import java.util.List;

public class PublisherMapper {


    public static Publisher convertToPlainModel(PublisherDb publisher) {
        return new Publisher(publisher.getId(), publisher.getName(), publisher.getWebsite(),
                UserMapper.convertToPlainModel(publisher.getUser()));
    }

    public static PublisherDb convertToDbModel(Publisher publisher) {
        return new PublisherDb(publisher.getId(), publisher.getName(), publisher.getWebsite(),
                UserMapper.convertToDbModel(publisher.getUser()));
    }

    public static List<Publisher> convertToListOfPlainModels(List<PublisherDb> publisherDbs) {
        List<Publisher> publishers = new ArrayList<>();
        for(PublisherDb publisherDb : publisherDbs){
         publishers.add(convertToPlainModel(publisherDb));
        }
        return publishers;
    }

    public static List<PublisherDb> convertToListOfDbModels(List<Publisher> publishers){
        List<PublisherDb> publisherDbs = new ArrayList<>();
        for(Publisher publisher : publishers){
            publisherDbs.add(convertToDbModel(publisher));
        }
        return publisherDbs;
    }
}