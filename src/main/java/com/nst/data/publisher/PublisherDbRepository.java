package com.nst.data.publisher;

import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherDataSource;
import com.nst.domain.publisher.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PublisherDbRepository implements PublisherRepository {

    @Autowired
    private PublisherDataSource dataSource;

    @Override
    public List<Publisher> findAll() {
        return dataSource.findAll();
    }

    @Override
    public Publisher save(Publisher publisher) {
        return dataSource.save(publisher);
    }

    @Override
    public Publisher findById(Integer id) {
        return dataSource.findById(id);
    }

    @Override
    public void delete(Publisher publisher) {
        dataSource.delete(publisher);
    }
}
