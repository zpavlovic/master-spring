package com.nst.data.publisher;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PublisherServiceImpl implements PublisherService {
	
	@Autowired
	public PublisherDao publisherRepository;

	@Override
	public PublisherDb save(PublisherDb publisher) {
		PublisherDb p = publisherRepository.save(publisher);
		return p;
	}

	@Override
	public void delete(PublisherDb publisher) {
		publisherRepository.delete(publisher.getId());
		
	}

	@Override
	public PublisherDb findOne(PublisherDb publisher) {
		return publisherRepository.findOne(publisher.getId());
	}

	@Override
	public List<PublisherDb> findAll() {
		return publisherRepository.findAll();
	}

	@Override
	public PublisherDb findById(long id) {
		return publisherRepository.findById(id);
	}

}
