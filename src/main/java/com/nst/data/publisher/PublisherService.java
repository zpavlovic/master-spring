package com.nst.data.publisher;

import java.util.List;

public interface PublisherService {
	
	PublisherDb save(PublisherDb publisher);
	
	void delete(PublisherDb publisher);
	
	PublisherDb findOne(PublisherDb publisher);
	
	List<PublisherDb> findAll();
	
	PublisherDb findById(long id);

}
