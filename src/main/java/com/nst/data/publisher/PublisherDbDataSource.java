package com.nst.data.publisher;

import com.nst.domain.publisher.Publisher;
import com.nst.domain.publisher.PublisherDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PublisherDbDataSource implements PublisherDataSource {

    @Autowired
    private PublisherService service;

    @Override
    public List<Publisher> findAll() {
        List<PublisherDb> publisherDbs = service.findAll();
        return PublisherMapper.convertToListOfPlainModels(publisherDbs);
    }

    @Override
    public Publisher save(Publisher publisher) {
        PublisherDb publisherDb = service.save(PublisherMapper.convertToDbModel(publisher));
        return PublisherMapper.convertToPlainModel(publisherDb);
    }

    @Override
    public Publisher findById(Integer id) {
        return null;
    }

    @Override
    public void delete(Publisher publisher) {

    }
}
