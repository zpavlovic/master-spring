package com.nst;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.nst.delivery.BookController;


@SpringBootApplication
public class Nst2Application {

	@Value("${image.location}")
	private String imageLocation;

	public static void main(String[] args) {
		SpringApplication.run(Nst2Application.class, args);
	}
	

	@Bean
	CommandLineRunner init() {
		return (args) -> {
			// Java 8 lambda used to create a Boot CommandLineRunner at startup which creates that folder.
			if(!Paths.get(imageLocation).toFile().exists()){
				Files.createDirectory(Paths.get(imageLocation));
			}
		};
	}
}
