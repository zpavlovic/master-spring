package com.nst.data.mapper;

import com.nst.Nst2ApplicationTests;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryMapper;
import com.nst.data.user.UserDb;
import com.nst.domain.category.Category;
import com.nst.domain.user.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CategoryMapperTest extends Nst2ApplicationTests{

    private CategoryMapper categoryMapper;
    private Category category;
    private User user;
    private CategoryDb categoryDb;
    private UserDb userDb;

    @Before
    public void setUp(){
        user = getUser();
        category = getCategory();
        userDb = getUserDb();
        categoryDb = getCategoryDb();
    }

    @Test
    public void testConvertedPlainModelNotNull(){
        Category category = categoryMapper.convertToPlainModel(categoryDb);
        assertNotNull(category);
    }

    @Test
    public void testConvertedPlainModelIdsAreEqual(){
        Category category = categoryMapper.convertToPlainModel(getCategoryDb());
        assertThat(category.getId(), is(categoryDb.getId()));
    }

    @Test
    public void testConvertedPlainModelNamesAreEqual(){
        Category category = categoryMapper.convertToPlainModel(categoryDb);
        assertThat(category.getName(), is(categoryDb.getName()));
    }

    @Test
    public void testConvertedPlainModelDescriptionsAreEqual(){
        Category category = categoryMapper.convertToPlainModel(categoryDb);
        assertThat(category.getDescription(), is(category.getDescription()));
    }

    @Test
    public void testConvertedPlainModelUserIsNotNull(){
        Category category = categoryMapper.convertToPlainModel(categoryDb);
        assertNotNull(category.getUser());
    }

    @Test
    public void testConvertedPlainModelUsersHaveSameId(){
        Category category = categoryMapper.convertToPlainModel(categoryDb);
        assertThat(category.getUser().getId(), is(categoryDb.getUser().getId()));
    }

    @Test
    public void testConvertedDbModelNotNull(){
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertNotNull(categoryDb);
    }

    @Test
    public void testConvertedDbModelIdsAreEqual(){
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertThat(category.getId(), is(categoryDb.getId()));
    }

    @Test
    public void testConvertedDbModelNamesAreEqual(){
        Category category = getCategory();
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertThat(category.getName(), is(categoryDb.getName()));
    }

    @Test
    public void testConvertedDbModelDescriptionsAreEqual(){
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertThat(category.getDescription(), is(categoryDb.getDescription()));
    }

    @Test
    public void testConvertedDbModelUserIsNotNull(){
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertNotNull(categoryDb.getUser());
    }

    @Test
    public void testConvertedDbModelUsersHaveSameId(){
        CategoryDb categoryDb = categoryMapper.convertToDbModel(category);
        assertThat(category.getUser().getId(), is(categoryDb.getUser().getId()));
    }

    private CategoryDb getCategoryDb() {
        return new CategoryDb(10l, "Category name", "Zoran", userDb);
    }

    private UserDb getUserDb() {
        return new UserDb(1, "Zookey", "pw", "Zoran", "Pavlovic");
    }

    private Category getCategory() {
        return new Category(10l, "Category name", "Zoran", user);
    }

    private User getUser() {
        return new User(1, "Zookey", "pw", "Zoran", "Pavlovic");
    }

}
