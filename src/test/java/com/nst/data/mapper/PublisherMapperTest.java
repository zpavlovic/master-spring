package com.nst.data.mapper;

import com.nst.data.publisher.PublisherDb;
import com.nst.data.publisher.PublisherMapper;
import com.nst.data.user.UserDb;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.user.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class PublisherMapperTest {

    private PublisherMapper mapper;
    private PublisherDb publisherDb;
    private Publisher publisher;
    private UserDb userDb;
    private User user;

    @Before
    public void setUp(){
        userDb = new UserDb(1, "Zookey", "pw", "Zoran", "Pavlovic");
        user = new User(1, "Zookey", "pw", "Zoran", "Pavlovic");
        publisherDb = new PublisherDb(1, "Test name", "test website", userDb);
        publisher = new Publisher(1, "Test name", "test website",user);
    }

    @Test
    public void testPlainModelNotNull(){
        Publisher publisher = PublisherMapper.convertToPlainModel(publisherDb);
        assertNotNull(publisher);
    }

    @Test
    public void testDbModelNotNull(){
        PublisherDb publisherDb = PublisherMapper.convertToDbModel(publisher);
        assertNotNull(publisherDb);
    }

    @Test
    public void testDbModelIdIsSame(){
        PublisherDb publisherDb = PublisherMapper.convertToDbModel(publisher);
        assertThat(publisherDb.getId(), is(publisher.getId()));
    }

    @Test
    public void testPlainModelIdIsSame(){
        Publisher publisher = PublisherMapper.convertToPlainModel(publisherDb);
        assertThat(publisher.getId(), is(publisherDb.getId()));
    }

    @Test
    public void testPlainModelUserNotNull(){
        Publisher publisher = PublisherMapper.convertToPlainModel(publisherDb);
        assertNotNull(publisher.getUser());
    }

    @Test
    public void testDbModelUserNotNull(){
        PublisherDb publisherDb = PublisherMapper.convertToDbModel(publisher);
        assertNotNull(publisherDb.getUser());
    }

    @Test
    public void testPlainModelUserNameNotNull(){
        Publisher publisher = PublisherMapper.convertToPlainModel(publisherDb);
        assertNotNull(publisher.getUser().getUsername());
    }

    @Test
    public void testDbModelUserNameNotNull(){
        PublisherDb publisherDb = PublisherMapper.convertToDbModel(publisher);
        assertNotNull(publisherDb.getUser().getUsername());
    }

}
