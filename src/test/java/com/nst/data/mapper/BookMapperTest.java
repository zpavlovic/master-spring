package com.nst.data.mapper;

import com.nst.Nst2ApplicationTests;
import com.nst.data.author.AuthorDb;
import com.nst.data.book.BookDb;
import com.nst.data.book.BookMapper;
import com.nst.data.category.CategoryDb;
import com.nst.data.publisher.PublisherDb;
import com.nst.data.user.UserDb;
import com.nst.domain.author.Author;
import com.nst.domain.book.Book;
import com.nst.domain.category.Category;
import com.nst.domain.publisher.Publisher;
import com.nst.domain.user.User;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class BookMapperTest extends Nst2ApplicationTests{

    private BookMapper bookMapper;

    private BookDb bookDb;
    private UserDb userDb;
    private CategoryDb categoryDb;
    private PublisherDb publisherDb;

    private Book book;
    private User user;
    private Category category;
    private Publisher publisher;

    @Before
    public void setUp(){
        userDb = new UserDb(1, "Zookey", "pw", "Zoran", "Pavlovic");
        publisherDb = new PublisherDb(1, "test", "www.google.ba", userDb);
        categoryDb = new CategoryDb(10l, "Category name", "Zoran", userDb);
        List<CategoryDb> categoryDbs = new ArrayList<>();
        categoryDbs.add(categoryDb);
        bookDb = new BookDb(1, "Test name", "description", "image link", getDate(), getDate(), 100, 1231,
                "Short description", 1111, 1111, "download link", userDb, new ArrayList<AuthorDb>(),
                categoryDbs, publisherDb);

        user = new User(1, "Zookey", "pw", "Zoran", "Pavlovic");
        publisher = new Publisher(1, "test", "www.google.ba", user);
        category = new Category(10l, "Category name", "Zoran", user);
        List<Category> categories = new ArrayList<>();
        categories.add(category);
        book = new Book(1, "Test name", "description", "image link", getDate(), getDate(), 100, 1231,
                "Short description", 1111, 1111, "download link", user, new ArrayList<Author>(),
                categories, publisher);
    }

    @Test
    public void testPlainModelNotNull(){
        Book book = BookMapper.convertToPlainModel(bookDb);
        assertNotNull(book);
    }

    @Test
    public void testPlainModelIdIsEqual(){
        Book book = BookMapper.convertToPlainModel(bookDb);
        assertThat(book.getId(), is(bookDb.getId()));
    }

    @Test
    public void testPlainModelNameIsEqual(){
        assertThat(BookMapper.convertToPlainModel(bookDb).getName(), is(bookDb.getName()));
    }

    @Test
    public void testPlainModelDescriptionIsEqual(){
        assertThat(BookMapper.convertToPlainModel(bookDb).getDescription(), is(bookDb.getDescription()));
    }

    @Test
    public void testPlainModelViewsIsEqual(){
        assertThat(BookMapper.convertToPlainModel(bookDb).getViews(), is(bookDb.getViews()));
    }

    @Test
    public void testDbModelNotNull(){
        BookDb bookDb = BookMapper.convertToDbModel(book);
        assertNotNull(bookDb);
    }

    @Test
    public void testDbModelIdIsEqual(){
        assertThat(BookMapper.convertToDbModel(book).getId(), is(bookDb.getId()));
    }

    @Test
    public void testDbModelNameIsEqual(){
        assertThat(BookMapper.convertToDbModel(book).getName(), is(bookDb.getName()));
    }

    @Test
    public void testDbModelDescription(){
        assertThat(BookMapper.convertToDbModel(book).getDescription(), is(bookDb.getDescription()));
    }

    @Test
    public void testDbModelViewIsEqual(){
        assertThat(BookMapper.convertToDbModel(book).getViews(), is(bookDb.getViews()));
    }

    @Test
    public void testDbModelCategoriesAreNotNull(){
        assertNotNull(BookMapper.convertToDbModel(book).getCategoryList());
    }

    @Test
    public void testDbModelCategoriesSizeIsEqual(){
        assertThat(BookMapper.convertToDbModel(book).getCategoryList().size(),
                is(bookDb.getCategoryList().size()));
    }

    @Test
    public void testDbModelCatogryNameIsEqual(){
        CategoryDb categoryDb = BookMapper.convertToDbModel(book).getCategoryList().get(0);
        assertThat(categoryDb.getName(), is(category.getName()));
    }

    @Test
    public void testPlainModelCategoryNameIsEqual(){
        Category category = BookMapper.convertToPlainModel(bookDb).getCategoryList().get(0);
        assertThat(category.getName(), is(categoryDb.getName()));
    }

    @Test
    public void testDbModelUserIsSameId(){
        UserDb currentUserDb = BookMapper.convertToDbModel(book).getUser();
        assertThat(currentUserDb.getId(), is(userDb.getId()));
    }

    @Test
    public void testPlainModelUserIsSameId(){
        User currentUser = BookMapper.convertToPlainModel(bookDb).getUser();
        assertThat(currentUser.getId(), is(user.getId()));
    }

    @Test
    public void testPlainModelPublisherIsNotNull(){
        assertNotNull(BookMapper.convertToPlainModel(bookDb).getPublisher());
    }

    @Test
    public void testPlainModelPublisherIdIsSame(){
        Publisher publisher = BookMapper.convertToPlainModel(bookDb).getPublisher();
        assertThat(publisher.getId(), is(bookDb.getPublisher().getId()));
    }

    @Test
    public void testConvertListOfBooksToPlainNotEmpty(){
        List<BookDb> bookDbs = new ArrayList<>();
        bookDbs.add(bookDb);
        bookDbs.add(bookDb);
        bookDbs.add(bookDb);

        List<Book> books = BookMapper.convertListOfPlainBooks(bookDbs);

        assertThat(books.isEmpty(), is(false));
    }

    @Test
    public void testConvertListOfPlainBooksToDbNotEmpty(){
        List<Book> books = new ArrayList<>();
        books.add(book);
        books.add(book);

        List<BookDb> bookDbs = BookMapper.convertToListOfDbModels(books);

        assertThat(bookDbs.isEmpty(), is((false)));
    }

    private Date getDate(){
        return new Date(2018, 7, 27);
    }

}
