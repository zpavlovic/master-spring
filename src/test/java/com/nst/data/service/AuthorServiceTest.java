package com.nst.data.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import com.nst.data.author.AuthorDb;
import com.nst.data.author.AuthorService;
import com.nst.data.user.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.data.user.UserDb;

public class AuthorServiceTest extends Nst2ApplicationTests{

	@Autowired
    AuthorService authorService;

	@Autowired
	private UserService userService;


	@Test
	public void testSaveAuthor() {

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		userService.save(currentUser);
		AuthorDb savedAuthor = authorService.save(new AuthorDb(1, "Ime autora", "Test", "Opis", "slika", "website", currentUser));
		assertNotNull(savedAuthor);

	}

	@Test
	public void testDeleteAuthor(){

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		AuthorDb savedAuthor = getAuthorDb(currentUser);
		assertNotNull(savedAuthor);

		authorService.delete(savedAuthor);		
		AuthorDb deletedAuthor = authorService.findById(savedAuthor.getId());
		assertNull(deletedAuthor);

	}

	private AuthorDb getAuthorDb(UserDb currentUser) {
		return authorService.save(new AuthorDb(1, "Ime autora", "Test", "Opis", "slika", "website", currentUser));
	}

	@Test
	public void testUpdateAuthor(){

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		userService.save(currentUser);
		AuthorDb savedAuthor = authorService.save(new AuthorDb(1, "Ime autora", "Test", "Opis", "slika", "website", currentUser));
		assertNotNull(savedAuthor);

		AuthorDb updateAuthor = authorService.save(savedAuthor);
		updateAuthor.setFirstName("New name");

		assertNotNull(updateAuthor);
		assertEquals(savedAuthor, updateAuthor);		

	}

	@Test
	public void testFindAllAuthors(){

		//Add author to database
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		userService.save(currentUser);
		AuthorDb savedAuthor = authorService.save(new AuthorDb(1, "Ime autora", "Test", "Opis", "slika", "website", currentUser));
		assertNotNull(savedAuthor);

		//Find all
		List<AuthorDb> authors = authorService.findAll();
		System.out.println("AUTHORS SIZE: "+String.valueOf(authors.size()));
		assertNotNull(authors);

		//Check if authors list is not empty. We should have 1 element
		assertThat(authors.isEmpty(), is(false));

	}
}
