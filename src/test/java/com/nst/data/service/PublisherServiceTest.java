package com.nst.data.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import com.nst.data.publisher.PublisherService;
import com.nst.data.user.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.data.publisher.PublisherDb;
import com.nst.data.user.UserDb;

public class PublisherServiceTest extends Nst2ApplicationTests {
	
	@Autowired
	PublisherService publisherService;
	
	@Autowired
	UserService userService;

	@Test
	public void testSave() {
		//Save user
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		
		//Save publisher with user
		PublisherDb publisher = new PublisherDb(1, "Test publisher", "test.com", savedUser);
		PublisherDb savedPublisher = publisherService.save(publisher);
		
		//Check if it is not null
		assertNotNull(savedPublisher);
	}
	
	@Test
	public void testUpdatePublisher(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		
		//Save publisher with user
		PublisherDb publisher = new PublisherDb(1, "Test publisher", "test.com", savedUser);
		PublisherDb savedPublisher = publisherService.save(publisher);
		
		savedPublisher.setName("Updated");
		PublisherDb updatedPublisher = publisherService.save(savedPublisher);
		
		assertEquals(updatedPublisher.getId(), savedPublisher.getId());
	}
	
	@Test
	public void testDeletePublisher(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		
		//Save publisher with user
		PublisherDb publisher = new PublisherDb(2, "Test publisher", "test.com", savedUser);
		PublisherDb savedPublisher = publisherService.save(publisher);
		
		publisherService.delete(savedPublisher);
		
		PublisherDb deletedPublisher = publisherService.findOne(savedPublisher);
		
		assertNull(deletedPublisher);
	}
	
	@Test
	public void testFindAllPublishers(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		
		//Save publisher with user
		PublisherDb publisher = new PublisherDb(2, "Test publisher", "test.com", savedUser);
		publisherService.save(publisher);
		
		List<PublisherDb> publishers = publisherService.findAll();
		
		assertThat(publishers.size(), is(2));
	}

}
