package com.nst.data.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.nst.data.author.AuthorDb;
import com.nst.data.author.AuthorService;
import com.nst.data.book.BookDb;
import com.nst.data.book.BookService;
import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryService;
import com.nst.data.publisher.PublisherService;
import com.nst.data.user.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.data.publisher.PublisherDb;
import com.nst.data.user.UserDb;


public class BookServiceTest extends Nst2ApplicationTests {

	@Autowired
	private BookService bookService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private PublisherService publisherService;

	@Rule
	public final ExpectedException exception = ExpectedException.none();


	@Test
	public void testSaveBook() {

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");

		List<CategoryDb> categories = setUpCategory(currentUser);

		List<AuthorDb> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);
	}

	@Test
	public void testDeleteBook(){

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");

		List<CategoryDb> categories = setUpCategory(currentUser);

		List<AuthorDb> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);

		bookService.delete(savedBook);

		BookDb deletedBook = bookService.findOne(savedBook);

		assertNull(deletedBook);
	}

	@Test
	public void testUpdateBook(){

		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");

		List<CategoryDb> categories = setUpCategory(currentUser);

		List<AuthorDb> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);	

		BookDb updateBook = bookService.findOne(savedBook);
		updateBook.setName("New name for update");

		assertNotNull(updateBook);
		assertEquals(savedBook, updateBook);
	}

	@Test
	public void findAllBooks(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");

		List<CategoryDb> categories = setUpCategory(currentUser);

		List<AuthorDb> authors = setUpAuthor(currentUser);

		java.sql.Date date = setUpDates();	

		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);

		assertNotNull(savedBook);	

		List<BookDb> books = bookService.findAll();
		System.out.println("BOOKS SIZE: "+String.valueOf(books.size()));
		assertThat(books.isEmpty(), is(false));
	}

	@Test
	public void testBookSearch(){

		//Add book to database
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		List<CategoryDb> categories = setUpCategory(currentUser);
		List<AuthorDb> authors = setUpAuthor(currentUser);
		java.sql.Date date = setUpDates();			
		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);
		assertNotNull(savedBook);	

		try {
			List<BookDb> books = bookService.search("name");
			assertNotNull(books);
			assertThat(books.isEmpty(), is(false));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("BookDb SearchBookInteractor fail");
		}
	}

	@Test
	public void testBookSearchEmpty() throws Exception{
		exception.expect(Exception.class);
		bookService.search("BookDb name111");
	}

	@Test
	public void testIncreaseBookViews(){
		//Add book to database
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		List<CategoryDb> categories = setUpCategory(currentUser);
		List<AuthorDb> authors = setUpAuthor(currentUser);
		java.sql.Date date = setUpDates();			
		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);
		assertNotNull(savedBook);	
				
		//Increase views
		long views = savedBook.getViews();
		views++;
		savedBook.setViews(views);
				
		//Get updated book and test it
		BookDb updatedBook = bookService.saveBook(savedBook);
		assertNotNull(updatedBook);
		assertEquals(updatedBook, savedBook);
	}
	
	@Test
	public void testFindOneById(){
		//Add book to database
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		List<CategoryDb> categories = setUpCategory(currentUser);
		List<AuthorDb> authors = setUpAuthor(currentUser);
		java.sql.Date date = setUpDates();			
		BookDb savedBook = saveBook(currentUser, date, date, authors, categories, 1);
		assertNotNull(savedBook);	
		
		BookDb findBook = bookService.findById(savedBook.getId());
		assertNotNull(findBook);
		assertEquals(findBook, savedBook);
	}
	
	@Test
	public void testGetTop5Books(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		List<CategoryDb> categories = setUpCategory(currentUser);
		List<AuthorDb> authors = setUpAuthor(currentUser);
		java.sql.Date date = setUpDates();
		
		
		BookDb firstBook = saveBook(currentUser, date, date, authors, categories, 30);
		assertNotNull(firstBook);	
		
		BookDb secondBook = saveBook(currentUser, date, date, authors, categories, 333);
		assertNotNull(secondBook);	
		

		BookDb thirdBook = saveBook(currentUser, date, date, authors, categories, 20);
		assertNotNull(thirdBook);	
		
		BookDb fourthBook = saveBook(currentUser, date, date, authors, categories, 100);
		assertNotNull(fourthBook);	
		

		BookDb fifthBook = saveBook(currentUser, date, date, authors, categories, 1);
		assertNotNull(fifthBook);	
		
		BookDb sixthBook = saveBook(currentUser, date, date, authors, categories, 100);
		assertNotNull(sixthBook);	

		BookDb seventhBook = saveBook(currentUser, date, date, authors, categories, 2);
		assertNotNull(seventhBook);	
		
		BookDb eightBook = saveBook(currentUser, date, date, authors, categories, 200);
		assertNotNull(eightBook);	
		
		List<BookDb> popularBooks = bookService.getPopularBooks();
		System.out.println("SIZE: "+popularBooks.size());
		assertThat(popularBooks.isEmpty(), is(false));
	}
	
	@Test
	public void testFindAllBooksByAuthor(){
		//Add author to database
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		List<CategoryDb> categories = setUpCategory(currentUser);
		List<AuthorDb> authors = setUpAuthor(currentUser);
		java.sql.Date date = setUpDates();
		
		
		BookDb firstBook = saveBook(currentUser, date, date, authors, categories, 30);
		assertNotNull(firstBook);	
		
		BookDb secondBook = saveBook(currentUser, date, date, authors, categories, 333);
		assertNotNull(secondBook);	
		
		List<BookDb> booksByAuthor = authors.get(0).getBookList();
		
		assertNotNull(booksByAuthor);
		
		
		
	}
	


	private BookDb saveBook(UserDb currentUser, Date publishedDate, Date addedDate, List<AuthorDb> authors, List<CategoryDb> categories, long views){
		PublisherDb publisher = new PublisherDb(1, "test publisher", "test.com", currentUser);
		PublisherDb savedPublisher = publisherService.save(publisher);
		BookDb book =
				new BookDb(1, "name", "opis", null, publishedDate, addedDate, 111, views, "short desc", 111, 222, "www.test.com", currentUser, authors, categories, savedPublisher);
		BookDb savedBook = bookService.saveBook(book);
		return savedBook;
	}

	private Date setUpDates() {
		Calendar calendar = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(calendar.getTime().getTime());
		return date;
	}

	private List<AuthorDb> setUpAuthor(UserDb currentUser) {
		AuthorDb savedAuthor = authorService.save(new AuthorDb(1, "Ime autora", "Test", "Opis", "slika", "website", currentUser));
		assertNotNull(savedAuthor);
		List<AuthorDb> authorsList = new ArrayList<>();
		authorsList.add(savedAuthor);
		return authorsList;
	}

	public List<CategoryDb> setUpCategory(UserDb currentUser){
		UserDb savedUser = userService.save(currentUser);
		CategoryDb savedCategory = categoryService.save(new CategoryDb(1, "Test category", "Test", savedUser));
		assertNotNull(savedCategory);	
		List<CategoryDb> categoryList = new ArrayList<>();
		categoryList.add(savedCategory);
		return categoryList;
	}

}
