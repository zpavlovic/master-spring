package com.nst.data.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import com.nst.data.category.CategoryDb;
import com.nst.data.category.CategoryService;
import com.nst.data.user.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.nst.Nst2ApplicationTests;
import com.nst.data.user.UserDb;

public class CategoryServiceTest extends Nst2ApplicationTests {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private UserService userService;

	@Test
	public void testSaveCategory() {		
		CategoryDb savedCategory = saveCategory();
		assertNotNull(savedCategory);		
	}
	
	@Test
	public void testDeleteCategory() {		
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		CategoryDb savedCategory = categoryService.save(new CategoryDb(5, "Test category", "Test", savedUser));
		assertNotNull(savedCategory);
		
		categoryService.delete(savedCategory);
		CategoryDb deletedCategory = categoryService.findOne(savedCategory);
		assertNull(deletedCategory);		
	}
	
	@Test
	public void testUpdatedCategory() {		
		CategoryDb savedCategory = saveCategory();
		assertNotNull(savedCategory);
		
		CategoryDb updatedCategory = categoryService.save(savedCategory);
		assertNotNull(updatedCategory);
		assertEquals(savedCategory, updatedCategory);		
	}
	
	@Test
	public void testFindAllCategories(){
		CategoryDb savedCategory = saveCategory();
		assertNotNull(savedCategory);
		
		List<CategoryDb> categories = categoryService.findAll();
		assertThat(categories.isEmpty(), is(false));
	}
	
	public CategoryDb saveCategory(){
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		CategoryDb savedCategory = categoryService.save(new CategoryDb(1, "Test category", "Test", savedUser));
		return savedCategory;
	}
	
	@Test
	public void find5Categories(){
		
		UserDb currentUser = new UserDb(1, "admin", "admin", "admin", "admin");
		UserDb savedUser = userService.save(currentUser);
		CategoryDb savedCategory = categoryService.save(new CategoryDb(1, "Test category", "Test", savedUser));
		
		CategoryDb savedCategory1 = categoryService.save(new CategoryDb(2, "Test category", "Test", savedUser));
		
		List<CategoryDb> categories = categoryService.find5Categories();
		
		assertThat(categories.size(), is(2));
		
	}

}
